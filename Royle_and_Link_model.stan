// Royle & Link 2006 model
  data {
  int<lower=0> S;
  int y[S];
  int<lower=0> K;
  real p10_max;
  }
  parameters {
  real<lower=0, upper=1> p11;
  real<lower=0, upper=p10_max> p10;
  real<lower=0, upper=1> psi;
  }
  transformed parameters {
  real<lower=0, upper=1> m1psi;
  m1psi = 1 - psi;
  }
  
  model{
  real log_p_12;
  real log_p_1;
  real log_p_2;
  psi ~ beta(1, 1);
  p11 ~ uniform(0, 1);
  p10 ~ uniform(0, p10_max);
  log_p_1  = log(psi);
  log_p_2  = log(m1psi);
  
  for(n in 1:S){
    real log_p[2];
    log_p[1] = log_p_1  + binomial_lpmf(y[n] | K, p11); // prob of true positives
    log_p[2] = log_p_2  + binomial_lpmf(y[n] | K, p10); // prob of false positives
    target += log_sum_exp(log_p);
  }
}
  
  generated quantities {
  real ppres[S]; //prob of presence
  for (i in 1:S){
  real a[S];
  real b[S];
  a[i] = psi * p11^y[i] * (1 - p11)^(K - y[i]);
  b[i] = m1psi * p10^y[i] * (1-p10)^(K - y[i]);
  ppres[i] = a[i] / (a[i] + b[i]);
  }
}

