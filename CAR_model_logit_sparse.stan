// CAR SODM.
// For speed reason, the implementation applies a sparse matrix representation of the CAR prior. 
functions {
  /**
  * adapted from http://mc-stan.org/users/documentation/case-studies/mbjoseph-CARStan.html authored by Max Joseph, under the BSD (3 clause) license.
  * Return the log probability of a proper conditional autoregressive (CAR) prior 
  * with a sparse representation for the adjacency matrix
  *
  * @param phi Vector containing the parameters with a CAR prior
  * @param tau Precision parameter for the CAR prior (real)
  * @param alpha Dependence (usually spatial) parameter for the CAR prior (real)
  * @param W_sparse Sparse representation of adjacency matrix (int array)
  * @param n Length of phi (int)
  * @param W_n Number of adjacent pairs (int)
  * @param D_sparse Number of neighbors for each location (vector)
  * @param lambda Eigenvalues of D^{-1/2}*W*D^{-1/2} (vector)
  *
  * @return Log probability density of CAR prior up to additive constant
  */
  real sparse_car_lpdf(vector phi, real tau, real alpha, 
  int[,] W_sparse, vector D_sparse, vector lambda, int n, int W_n) {
  row_vector[n] phit_D; // phi' * D
      row_vector[n] phit_W; // phi' * W
  vector[n] ldet_terms;
  
  phit_D = (phi .* D_sparse)';
      phit_W = rep_row_vector(0, n);
      for (i in 1:W_n) {
        phit_W[W_sparse[i, 1]] = phit_W[W_sparse[i, 1]] + phi[W_sparse[i, 2]];
        phit_W[W_sparse[i, 2]] = phit_W[W_sparse[i, 2]] + phi[W_sparse[i, 1]];
      }
    
      for (i in 1:n) ldet_terms[i] = log1m(alpha * lambda[i]);
      return 0.5 * (n * log(tau)
                    + sum(ldet_terms)
                    - tau * (phit_D * phi - alpha * (phit_W * phi)));
  }
}
data {
  int<lower=0> S;
  int y[S];
  int<lower=0> K;
  real p10_max;
  matrix<lower = 0, upper = 1>[S, S] W; //adjacency matrix
  int<lower=1> n;
  int W_n;                // number of adjacent region pairs
  }
  transformed data{
  int W_sparse[W_n, 2];   // adjacency pairs
  vector[n] D_sparse;     // diagonal of D (number of neigbors for each site)
  vector[n] lambda;       // eigenvalues of invsqrtD * W * invsqrtD

  { // generate sparse representation for W
    int counter;
    counter = 1;
    // loop over upper triangular part of W to identify neighbor pairs
    for (i in 1:(n - 1)) {
      for (j in (i + 1):n) {
        if (W[i, j] == 1) {
          W_sparse[counter, 1] = i;
          W_sparse[counter, 2] = j;
          counter = counter + 1;
        }
      }
    }
  }
  for (i in 1:n) D_sparse[i] = sum(W[i]);
  {
    vector[n] invsqrtD;  
    for (i in 1:n) {
    invsqrtD[i] = 1 / sqrt(D_sparse[i]);
    }
    lambda = eigenvalues_sym(quad_form(W, diag_matrix(invsqrtD)));
  }
}
  parameters {
  real<lower=0, upper=1> p11;
  real<lower=0, upper=p10_max> p10;
  real<lower=0, upper=1> psi;
  real<lower=-1, upper=1> alpha;
  vector[S] phi; // autoregressive term
  real<lower=0> sigma; // one over tau square
  }
  transformed parameters {
  real<lower=0, upper=1> m1psi;
  real<lower = 0> tau;
  m1psi = 1 - psi;
  tau = 1 / sigma^2;
  }
  model{
  real log_p_12;
  real log_p_1;
  real log_p_2;
  vector[S] psi1;
  psi ~ beta(1, 1);
  p11 ~ uniform(0, 1);
  p10 ~ uniform(0, p10_max);
  phi ~ sparse_car(tau, alpha, W_sparse, D_sparse, lambda, n, W_n); 
  alpha ~ uniform(-1, 1);
  sigma ~ gamma(2, 2);
  psi1 = inv_logit(logit(psi) + phi);
  
  for(i in 1:S){
    real log_p[2];
    log_p_1  = log(psi1[i]);
    log_p_2  = log1m(psi1[i]);
    log_p[1] = log_p_1  + binomial_lpmf(y[i] | K, p11); // prob of true positives
    log_p[2] = log_p_2  + binomial_lpmf(y[i] | K, p10); // prob of false positives
    target += log_sum_exp(log_p);
  }
}
  
  generated quantities {
  real ppres[S]; // probability of presence
  vector[S] psi1;
  psi1 = inv_logit(logit(psi) + phi);
  for (i in 1:S){
  real a[S];
  real b[S];
  a[i] = psi1[i] * p11^y[i] * (1 - p11)^(K - y[i]);
  b[i] = (1 - psi1[i]) * p10^y[i] * (1-p10)^(K - y[i]);
  ppres[i] = a[i] / (a[i] + b[i]);
  }
}

