# eDNA data analysis using Species Occupancy-Detection Models (SODMs)
[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

To perform the MCMC sampling for the SODMs, please install the R package **rstan**. Instructions to install the package can be found at https://github.com/stan-dev/rstan/wiki/Installing-RStan-on-Mac-or-Linux for Mac or Linux environments, and https://github.com/stan-dev/rstan/wiki/Installing-RStan-on-Windows for Windows environments.

* **ts_analysis.R**: R code to replicate the analysis for the Giguet-Covex et al. 2014 livestock eDNA data (http://doi.org/10.5061/dryad.h11h7), as an example to the use of SODMs on eDNA chronosequences; age data are in the **anterne_sample_ages.tab** file in this repository; **CAR_model_logit_sparse.stan** and **Royle_and_Link_model.stan** are required in order to perform MCMC sampling;
* **spatial_analysis.R**: R code to replicate the analysis for the Dougherty et al. 2016 crayfish eDNA data (http://doi.org/10.5061/dryad.3hp15), as an example to the use of SODMs on spatial eDNA data; **CAR_model_logit_sparse.stan** and **Royle_and_Link_model.stan** are required in order to perform MCMC sampling;
* **CAR_model_logit_sparse.stan**: stan model code of a Conditionally AutoRegressive model using sparse representation for the precision matrix in multivariate normal distribution;
* **CAR_model_multinormal.stan**: stan model code of a Conditionally AutoRegressive model using system-provided multivariate normal distribution function, much slower;
* **Royle_and_Link_model.stan**: stan model code implementing the Royle and Link (2006) model;
* **eDNA_simulation.R**: R code to generate the simulation data presented in the main text; the required spatial dataset **snouterdata.txt** can be found at http://www.ecography.org/appendix/e5171;
* **anterne_sample_ages.tab**: A data table containing sample ages required in **ts_analysis.R**. 
