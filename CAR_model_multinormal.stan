// CAR SODM using multivariate normal distribution provided by the system, a slow implementation
data {
  int<lower=0> S;
  int y[S];
  int<lower=0> K;
  real p10_max;
  matrix<lower = 0, upper = 1>[S, S] W; //adjacency matrix
  int<lower=1> n;
  int W_n;                // number of adjacent region pairs
}
transformed data{
  matrix<lower = 0>[S, S] D;
  vector[S] zeros;
  
  zeros = rep_vector(0, S);
  {
    vector[S] W_rowsums;
  
    for (i in 1:S) {
      W_rowsums[i] = sum(W[i, ]);
    }
    D = diag_matrix(W_rowsums);
  }
}
  parameters {
  real<lower=0, upper=1> p11;
  real<lower=0, upper=p10_max> p10;
  real<lower=0, upper=1> psi;
  real<lower=-1, upper=1> alpha;
  vector[S] phi; // autoregressive term
  real<lower=0> sigma; // one over tau square
  }
  transformed parameters {
  real<lower=0, upper=1> m1psi;
  real<lower = 0> tau;
  m1psi = 1 - psi;
  tau = 1 / sigma^2;
  }
  model{
  real log_p_12;
  real log_p_1;
  real log_p_2;
  vector[S] psi1;
  
  psi ~ beta(1, 1);
  p11 ~ uniform(0, 1); // can be adjusted eventually
  p10 ~ uniform(0, p10_max);
  phi ~ multi_normal_prec(zeros,  tau * (D - alpha * W));
  alpha ~ uniform(-1, 1);
  sigma ~ gamma(2, 2);
  
  psi1 = inv_logit(logit(psi) + phi);
  
  for(i in 1:S){
    real log_p[2];
    log_p_1  = log(psi1[i]);
    log_p_2  = log1m(psi1[i]);
    log_p[1] = log_p_1  + binomial_lpmf(y[i] | K, p11); // prob of true positives
    log_p[2] = log_p_2  + binomial_lpmf(y[i] | K, p10); // prob of false positives
    target += log_sum_exp(log_p);
  }
}
  
  generated quantities {
  real ppres[S]; // probability of presence
  vector[S] psi1;
  psi1 = inv_logit(logit(psi) + phi);
  for (i in 1:S){
  real a[S];
  real b[S];
  a[i] = psi1[i] * p11^y[i] * (1 - p11)^(K - y[i]);
  b[i] = (1 - psi1[i]) * p10^y[i] * (1-p10)^(K - y[i]);
  ppres[i] = a[i] / (a[i] + b[i]);
  }
}

